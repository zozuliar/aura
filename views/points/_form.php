<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper as ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\Markers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="markers-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'country')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?php
        
        $placeTypes = \app\models\Placetypes::find()->all();

        $items = ArrayHelper::map($placeTypes,'id','title');
        $params = [
            'prompt' => 'Set place type, please'
        ];
        echo $form->field($model, 'place_type_id')->dropDownList($items,$params);
    ?>

    <?= $form->field($model, 'marker_icon')->textInput() ?>
OR
    <?= $form->field($model, 'imageFile')->fileInput() ?>
    
    <?= $form->field($model, 'long')->textInput() ?>

    <?= $form->field($model, 'lat')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
