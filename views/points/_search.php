<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper as ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\MarkersQuery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="markers-search">
<form action="">

    <input type="search" name="city" value="<?= (array_key_exists(':city', $where['params']))?$where['params'][':city']:''; ?>" placeholder="City">
    <select name="place_type_id">
        <option value="">Filtr by Place Type</option>
    <?php
        
        $placeTypes = \app\models\Placetypes::find()->all();

        $items = ArrayHelper::map($placeTypes,'id','title');
        $params = [
            'prompt' => 'Set place type, please'
        ];
        foreach($items as $key=> $value){
            if (array_key_exists(':placetype_id', $where['params']) && ($where['params'][':placetype_id'] == $key))
                $html_selected = ' selected';
            else
                $html_selected = '';
            echo "<option value=\"$key\"$html_selected>$value</option>";
        }
    ?>
    </select>
    <input type="search" name="long" value="<?= (array_key_exists(':long', $where['params']))?$where['params'][':long']:''; ?>" placeholder="longitude">
    <input type="search" name="lat" value="<?= (array_key_exists(':lat', $where['params']))?$where['params'][':lat']:''; ?>" placeholder="latitude">
    <input type="search" name="radius" value="<?= (array_key_exists(':radius', $where['params']))?$where['params'][':radius']:''; ?>" placeholder="radius">
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

</form>
</div>
