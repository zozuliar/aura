<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Placetypes */

$this->title = Yii::t('app', 'Create Placetypes');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Placetypes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="placetypes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
