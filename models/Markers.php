<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
/**
 * This is the model class for table "markers".
 *
 * @property integer $id
 * @property string $country
 * @property string $city
 * @property string $address
 * @property integer $place_type_id
 * @property resource $marker_icon
 * @property integer $long
 * @property integer $lat
 *
 * @property Placetypes $placeType
 */
class Markers extends \yii\db\ActiveRecord
{
    
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'markers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country', 'city', 'address', 'place_type_id', 'long', 'lat'], 'required'],
            [['place_type_id'], 'integer'],
            [['long', 'lat'], 'double'],
            [['marker_icon'], 'string', 'max' => 30],
            [['country', 'city'], 'string', 'max' => 100],
            [['address'], 'string', 'max' => 255],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'gif, png, jpg, ico'],
            [['place_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Placetypes::className(), 'targetAttribute' => ['place_type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country' => 'Country',
            'city' => 'City',
            'address' => 'Address',
            'place_type_id' => 'Place Type ID',
            'marker_icon' => 'Marker Icon',
            'long' => 'Long',
            'lat' => 'Lat',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlacetype()
    {
        return $this->hasOne(Placetypes::className(), ['id' => 'place_type_id']);
    }

    /**
     * @inheritdoc
     * @return MarkersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MarkersQuery(get_called_class());
    }
    
    public function upload()
    {
        if (!empty($this->imageFile)) {
            $file_name = __DIR__ . '/../web/icons/' . $this->id . '.' . $this->imageFile->extension;
            //die($file_name);
            $this->imageFile->saveAs($file_name);
            $this->marker_icon = '/icons/' . $this->id . '.' . $this->imageFile->extension;
            $this->imageFile = '';
            return true;
        } else {
            return false;
        }
    }
    
}
