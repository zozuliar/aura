<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Placetypes]].
 *
 * @see Placetypes
 */
class PlacetypesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Placetypes[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Placetypes|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
