<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "placetypes".
 *
 * @property integer $id
 * @property string $title
 *
 * @property Markers[] $markers
 */
class Placetypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'placetypes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Place type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarkers()
    {
        return $this->hasMany(Markers::className(), ['place_type_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return PlacetypesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PlacetypesQuery(get_called_class());
    }
}
