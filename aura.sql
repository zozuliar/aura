-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Час створення: Сер 27 2017 р., 13:48
-- Версія сервера: 5.5.53
-- Версія PHP: 5.6.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `aura`
--

-- --------------------------------------------------------

--
-- Структура таблиці `markers`
--

CREATE TABLE `markers` (
  `id` int(11) NOT NULL,
  `country` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `place_type_id` int(11) NOT NULL,
  `marker_icon` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `long` float NOT NULL,
  `lat` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `markers`
--

INSERT INTO `markers` (`id`, `country`, `city`, `address`, `place_type_id`, `marker_icon`, `long`, `lat`) VALUES
(1, 'Ukraine', 'Gaj', 'Gaj village', 2, '/icons/1.jpg', 30, 50),
(2, 'Ukraine', 'Kyiv', 'Golosiivska, 33', 1, '/icons/2.jpg', 30.5, 50.1),
(3, 'Ukraine', 'Kyiv', 'Vasylkivska,25', 3, '/icons/3.jpg', 30.45, 50.09),
(4, 'Ukraine', 'Kyiv', 'Vasylkivska,45', 2, '/icons/4.jpg', 30.45, 50.1),
(5, 'Ukraine', 'Kyiv', 'Golosiivska, 77', 1, '/icons/5.jpg', 50.1, 30.44),
(6, 'Ukraine', 'Kyiv', 'Vasylkivska,77', 3, '/icons/6.jpg', 50.21, 30.22),
(8, 'Ukraine', 'Kyiv', 'Zhilyanska, 33', 2, '', 30.2, 50.75),
(12, 'Ukraine', 'Kyiv', 'Zhilyanska, 33', 1, '/icons/12.jpg', 30.226, 50.75),
(13, 'Ukraine', 'Kyiv', 'Zhilyanska, 33', 1, '/icons/13.jpg', 30.226, 50.75),
(14, 'Ukraine', 'Kyiv', 'Zhilyanska, 33', 1, '/icons/14.jpg', 30.226, 50.75),
(15, 'Ukraine', 'Kyiv', 'Zhilyanska, 35', 3, '/icons/15.jpg', 30.221, 50.75),
(16, 'test', 'test', 'test', 1, 'test', 77.1539, 120.398),
(17, 'test', 'test', 'Gaj village', 3, 'test', 120.398, 77.1539);

-- --------------------------------------------------------

--
-- Структура таблиці `placetypes`
--

CREATE TABLE `placetypes` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп даних таблиці `placetypes`
--

INSERT INTO `placetypes` (`id`, `title`) VALUES
(1, 'shop'),
(2, 'house'),
(3, 'pharmacy');

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `markers`
--
ALTER TABLE `markers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-marker-placetype` (`place_type_id`);

--
-- Індекси таблиці `placetypes`
--
ALTER TABLE `placetypes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `markers`
--
ALTER TABLE `markers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT для таблиці `placetypes`
--
ALTER TABLE `placetypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Обмеження зовнішнього ключа збережених таблиць
--

--
-- Обмеження зовнішнього ключа таблиці `markers`
--
ALTER TABLE `markers`
  ADD CONSTRAINT `fk-marker-placetype` FOREIGN KEY (`place_type_id`) REFERENCES `placetypes` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
