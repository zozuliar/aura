REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.6.0.


INSTALLATION
------------

### Install via Composer and bitbucket

1) Checkout to the directory, containing your sites, from bitbucket link: 
~~~
git clone https://<your_bitbucket_login>@bitbucket.org/zozuliar/aura.git
~~~

2) If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

3) Use the following command:

~~~
php composer.phar global require "fxp/composer-asset-plugin:^1.3.1"
php composer.phar update
~~~

4) Create mysql database and import aura.sql from root of bitbucket repository to your mysql database.

5) Set proper mysql credentials in /config/db.php file

6) Setup new virtual host and make /web subdirectory root of your site

Now you should be able to access the application through the following URL, assuming `aura` is the directory
directly under the Web root.

~~~
http://aura ,
~~~
where http://aura - your test domain url.


REST ACCESS
___________

Rest API will be available via url 
~~~
http://aura/markers ,
~~~
where http://aura - your test domain url.
