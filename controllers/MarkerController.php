<?php

namespace app\controllers;
 
use Yii;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\helpers\Json;
use app\models\Markers as Markers;
 
class MarkerController extends ActiveController
{
    public $modelClass = 'app\models\Markers';
    
    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
            'view' => ['GET', 'HEAD'],
            'create' => ['POST'],
            'update' => ['PUT', 'UPDATE'],
            'delete' => ['DELETE'],
        ];
    }
    
    public static function getSqlWhereForNearby($lat, $long, $radius)
    {
        $lat_min = $lat - (360/39600000)*$radius;
        $lat_max = $lat + (360/39600000)*$radius;

        $latitude_length = 39600000*cos(abs($lat_min)*pi()/180);
        $long_min = $long - (360/$latitude_length)*$radius;
        $long_max = $long + (360/$latitude_length)*$radius;
        
        return array('sql'=>'(lat BETWEEN :lat_min AND :lat_max)'.
                ' AND (`long` BETWEEN :long_min AND :long_max)' .
                ' AND (2*ASIN(SQRT(POW(SIN((RADIANS(lat) - RADIANS(:lat))/2), 2) + COS(RADIANS(lat))*COS(RADIANS(:lat))*POW(SIN((((RADIANS(`long`) - RADIANS(:long))/2))),2)))*6372795 <= :radius)',
                'params'=>[
                    ':lat_min' => $lat_min,
                    ':lat_max' => $lat_max,
                    ':long_min' => $long_min,
                    ':long_max' => $long_max,
                    ':long' => $long,
                    ':lat' => $lat,
                    ':radius' => $radius
                    ]
            );
    }
    
    public function actionNearby()
    {
        $get_array = Yii::$app->request->get();
        if (!isset($get_array['lat']) || !is_numeric($get_array['lat']))
            die('Bad Latitude');
        if (!isset($get_array['long']) || !is_numeric($get_array['long']))
            die('Bad Longitude');
        if (!isset($get_array['radius']) || !is_numeric($get_array['radius']))
            die('Bad Radius');
        
        $where = self::getSqlWhereForNearby($get_array['lat'], $get_array['long'], $get_array['radius']);
        $query = Markers::find()->where($where['sql'], $where['params']);
        //Yii::$app->response->format = Response::FORMAT_JSON;
        return $query->all();
        //die($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);
    }
}