<?php

namespace app\controllers;

use Yii;
use app\models\Markers;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MarkersController implements the CRUD actions for Markers model.
 */
class PointsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Markers models.
     * @return mixed
     *
    public function actionIndex()
    {
        $query = Markers::find()->joinWith(['placetype']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                    'pageSize' => 10,
                ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }*/

    /**
     * Displays a single Markers model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $query = $this->findModel($id);
        $query->getPlacetype();
        return $this->render('view', [
            'model' => $query,
        ]);
    }

    /**
     * Creates a new Markers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Markers();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->imageFile = \yii\web\UploadedFile::getInstance($model, 'imageFile');
            if ($model->upload()){
                $model->save();
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Markers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $model->imageFile = \yii\web\UploadedFile::getInstance($model, 'imageFile');
            $model->upload();
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
        else {
                return $this->render('update', [
                    'model' => $model,
                ]);
        }
    }

    /**
     * Deletes an existing Markers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $fn = __DIR__ . '/../web' . $model->marker_icon;
        if (file_exists($fn))
        {    
            unlink($fn);
        }
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Markers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Markers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Markers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionIndex()
    {
        $get_array = Yii::$app->request->get();
        
        $b_nearby_filtr = (isset($get_array['lat']) && is_numeric($get_array['lat'])) &&
        (isset($get_array['long']) && is_numeric($get_array['long'])) &&
        (isset($get_array['radius']) && is_numeric($get_array['radius']));
        
        if($b_nearby_filtr){
            $where=\app\controllers\MarkerController::getSqlWhereForNearby($get_array['lat'], $get_array['long'], $get_array['radius']);
            $sql_delimeter = ' AND';
        }
        else{
            $where = ['sql'=>'', 'params'=>array()];
            $sql_delimeter = '';
        }
        
        if (($get_array['place_type_id']) || is_numeric($get_array['place_type_id'])){
            $where['sql'] .= $sql_delimeter . ' place_type_id=:placetype_id';
            $sql_delimeter = ' AND';
            $where['params'][':placetype_id'] = $get_array['place_type_id'];
        }

        if (!empty($get_array['city'])){
            $where['sql'] .= $sql_delimeter . ' city=:city';
            $sql_delimeter = ' AND';
            $where['params'][':city'] = $get_array['city'];
        }

        $query = Markers::find()->where($where['sql'], $where['params']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                    'pageSize' => 10,
                ],
        ]);

        
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'filtr' => $model_filtr,
            'where' => $where,
            'sql' => $query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql
        ]);
    }
}
